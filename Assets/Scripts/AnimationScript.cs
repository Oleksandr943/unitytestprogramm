﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour {

    Animator An;

	// Use this for initialization
	void Start () {
        An = GetComponent<Animator>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            An.SetBool("MovementPerfomed", true);
        }
        else
        {
            An.SetBool("MovementPerfomed", false);
        }
	}
}
