﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandScript : MonoBehaviour {

    Rigidbody2D RB;
    Vector2 Direction = new Vector2(0,1);

	void Start () {
        RB = GetComponent<Rigidbody2D>();
        
	}
	
	void Update () {
        RB.AddForce(Direction*RB.mass*9.81f*11);
	}
}
